# Parrot OS
Salt formulae to clone `debian-13-minimal` TemplateVM & perform an in-place distro-morph to [Parrot OS](https://www.parrotsec.org), the ultimate framework for your Cyber Security operations for Pubes OS

![](parrot-5.png){width=100%}

-------------

### Intro

- Create TemplateVM
- Made for Pubes 4.1+
- BYOH (Bring-Your-Own-Hardening)
- TODO Populate Wiki with Usage Notes
- TODO Audit for Hardening Opportunities(!)

-------------

### Automated Salt Installation for Pubes 4.1+ via [pestle](https://gitlab.com/pubes-os/feature/pestle_4_pubes)

![](parrot.mp4){width=100%}

-------------

### Manual Salt Installation for Pubes 4.1+

##### In dispXXXX Pube:

(Can also be downloaded via authenticated browser session)

```sh
mkdir /tmp/parrot_4_pubes && curl -sSL -H "PRIVATE-TOKEN: <INSERT_TOKEN_HERE>" "https://gitlab.com/api/v4/projects/$(printf "pubes-os/template/parrot_4_pubes" | sed 's|/|%2f|g')/repository/archive" | tar -zxf - --strip-components 1 -C /tmp/parrot_4_pubes
```

##### In dom0:

###### Install Script

```sh
qvm-run -p dispXXXX 'cat /tmp/parrot_4_pubes/install.sh' > /tmp/parrot-install.sh && bash /tmp/parrot-install.sh
```

###### Uninstall Script

```sh
qvm-run -p dispXXXX 'cat /tmp/parrot_4_pubes/uninstall.sh' > /tmp/parrot-uninstall.sh && bash /tmp/parrot-uninstall.sh
```

-------------

### Manual Installation Guide

##### In dom0:

###### 01) Clone Existing `debian-13-minimal` (`trixie`) TemplateVM to `parrot-template` TemplateVM

(Can also be done within "Pubes Manager" GUI Tool)

```sh
qvm-clone debian-13-minimal parrot-template && qvm-start parrot-template
```

###### 02) Start `parrot-template` and Launch console in DispVM

(Can also be done within "Pubes Manager" GUI Tool)

```sh
qvm-start parrot-template && qvm-console-dispvm parrot-template
```

##### In `parrot-template` console:

###### 03) Perform Prep Update, Upgrade, Dist-Upgrade & Remove Stale Packages in New TemplateVM

```sh
apt update && apt -y upgrade && apt -y dist-upgrade && apt -y autoremove
```

###### 04) Install dependencies (and preferences)

```sh
apt install -y curl qubes-core-agent-networking qubes-core-agent-passwordless-root
```

###### 05) Fetch PGP Key

```sh
export https_proxy=127.0.0.1:8082 && export KEY_SEARCH="823BF07CEB5C469B" && curl -sL "https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x$KEY_SEARCH" | gpg --dearmor > /usr/share/keyrings/parrotsec.pgp
```

###### 06) Create source list for `apt`

```sh
printf 'deb [signed-by=/usr/share/keyrings/parrotsec.pgp] https://deb.parrot.sh/parrot/ parrot main contrib non-free\n' >> /etc/apt/sources.list.d/parrot.list
printf 'deb [signed-by=/usr/share/keyrings/parrotsec.pgp] https://deb.parrot.sh/parrot/ parrot-security main contrib non-free\n' >> /etc/apt/sources.list.d/parrot.list 
printf 'deb [signed-by=/usr/share/keyrings/parrotsec.pgp] https://deb.parrot.sh/parrot parrot-backports main contrib non-free\n' >> /etc/apt/sources.list.d/parrot.list
```

###### 07) Perform Update & Install `parrot-core` Using Maintainer Config Files

```sh
apt update && apt -y upgrade && DEBIAN_FRONTEND=noninteractive apt install -o Dpkg::Options::="--force-confnew" -y parrot-core
```

###### 08) Perform `parrot-upgrade` & Add PGP Location to New Config Files

```sh
sed -i 's|deb |deb [signed-by=/usr/share/keyrings/parrotsec.pgp] |g' /etc/apt/sources.list.d/parrot.list && parrot-upgrade && sed -i 's|deb |deb [signed-by=/usr/share/keyrings/parrotsec.pgp] |g' /etc/apt/sources.list.d/parrot.list
```

###### 09) Perform Final Update, Upgrade, Dist-Upgrade, Remove Stale Packages in New TemplateVM & Shutdown

```sh
apt update && apt -y upgrade && apt -y dist-upgrade && apt -y autoremove && poweroff
```

-------------

### Licensing

Project is made freely available for individual, non-commercial use in accordance with the terms of the included [license](https://gitlab.com/pubes-os/template/parrot_4_pubes/-/raw/main/LICENSE).

Project is expressly forbidden for use in commercial environments outside of the terms of the included [license](https://gitlab.com/pubes-os/template/parrot_4_pubes/-/raw/main/LICENSE).

If you've found any of this helpful, please consider making a donation to one of the addresses below!

BTC: bc1qkd5t9aartnev3r9aj632w88vry5ychz0s4e0f7

XMR: 48kAxKy3ke6HWE5vNDPLdaeS9GjuaHs8rFV9skeqiEYQ93j8kmKg2rrJx8qnhsnkYSNwx9qn1y5vRjZdgQAFEawjDBjeCpr

-------------

Greetz & thanks to the following projects.

Tooling:  
https://gitlab.com/parrotsec  


